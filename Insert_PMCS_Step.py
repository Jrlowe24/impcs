import sys
import mysql.connector as mariadb
import os
import subprocess

username = ""
password = ""

def commitDB():
    #os.system("bash %s > /venv")
    #os.system("chmod +x update-db.sh %s > /venv")
    #os.system("./update-db.sh %s > /venv")
    #os.system("update-db.sh %s > /venv")
    cmd2 = "update-db.sh"
    cmd3 = "chmod +x update-db.sh"
    cmd4 = "./update-db.sh"
    returned_value = subprocess.call(cmd2, shell=True)
    # returns the exit code in unix
    #print('returned value:', returned_value)


def sqldump():
    #os.system("bash %s > /venv")
    #os.system("chmod +x sql-dump.sh %s > /venv")
    #os.system("./sql-dump.sh %s > /venv")
    #os.system("sql-dump.sh %s > /venva")
    cmd2 = "sql-dump.sh"
    cmd3 = "chmod +x sql-dump.sh"
    cmd4 = "./sql-dump.sh"
    returned_value = subprocess.call(cmd2, shell=True)  # returns the exit code in unix
    #print('returned value:', returned_value)


def main():
    login(username, password)


def login(username, password):
    username = input("Enter your database username: ")
    password = input("Enter your password: ")
    print("Welcome - Access Granted")
    menu(username, password) 


def menu(username, password):
    print(30 * "-", "MENU", 30 * "-")
    # time.sleep(1)
    print()

    choice = input("""
    A: Enter entries into an iPMCS database 
    B: Enter custom MySQL command
    Q: Quit/Log Out
    Please enter your choice: """)
    print(66 * "-")
    if choice == "A" or choice == "a":
        enter_step(username, password)
        menu(username, password)
    elif choice == "B" or choice == "b":

        enter_query(username, password)
        menu(username, password)
    elif choice == "Q" or choice == "q":
        commitDB()
        sys.exit
    else:
        print("You must only select either A,B,C, or D.")
        print("Please try again")
        menu(username, password)


def enter_step(username, password):
    # my_db = mariadb.connect(user='pi', password='root', database='iPMCS')
    # cursor = my_db.cursor()
    try:
        my_db = mariadb.connect(user=username, password=password, database='iPMCS')
        cursor = my_db.cursor()
    except mariadb.Error as error:
        my_db.rollback()  # rollback if any exception occured
        print("Failed to connect to database with those credentials, please try again {}".format(error))

    print(24 * "-", "AVAILABLE TABLES", 24 * "-")
    cursor.execute("show tables;")
    result = cursor.fetchall()
    for row in result:
        print(row[0])
    print(66 * "-")
    table = input('Enter the table you want to input to: ')
    if table == 'hmmwv':
        try:
            sql = "INSERT INTO hmmwv (interval_, location_, procedure_, not_FMC) VALUES (%s, %s, %s, %s)"
            interval_ = input('Enter interval: ')
            location_ = input('Enter Location/Item to Check/Service: ')
            procedure_ = input('Enter Crewmember Procedure: ')
            not_FMC = input('Enter Not Fully Mission Capable if: ')
            val = (interval_, location_, procedure_, not_FMC)
            cursor.execute(sql, val)
            my_db.commit()
            sqldump()
            print(cursor.rowcount, "record inserted.")
        except mariadb.Error as error:
            my_db.rollback() #rollback if any exception occured
            print("Failed inserting record into hmmwv table {}".format(error))
    elif table == 'users':
        try:
            user_id = input('Enter User ID: ')
            u_password = input('Enter User Password: ')
            check = input("Is the user an admin (enter 1 if he is, 0 if not): ")
            if check == '1':
                is_Admin = 1
            else:
                is_Admin = 0
            val1 = (user_id, u_password, is_Admin)
            sql1 = "INSERT INTO users (user_id, u_password, is_Admin) VALUES (%s, %s, %s)"
            cursor.execute(sql1, val1)
            my_db.commit()
            sqldump()
            print(cursor.rowcount, "record inserted.")
        except mariadb.Error as error:
            my_db.rollback() #rollback if any exception occured
            print("Failed inserting record into users table {}".format(error))
    elif table == 'vehicle':
        try:
            sql2 = "INSERT INTO vehicle (vehicle_type, vehicle_id, fmc_status) VALUES (%s, %s, %s)"
            vehicle_type = input('Enter vehicle type: ')
            vehicle_id = input('Enter Vehicle ID: ')
            check1 = input('Enter if vehicle is Fully Mission Capable(yes or no): ')
            if check1 == 'yes':
                fmc_status = 1
            else:
                fmc_status = 0
            val2 = (vehicle_type, vehicle_id, fmc_status)
            cursor.execute(sql2, val2)
            my_db.commit()
            sqldump()
            print(cursor.rowcount, "record inserted.")
        except mariadb.Error as error:
            my_db.rollback() #rollback if any exception occured
            print("Failed inserting record into vehicle table {}".format(error))
    pass


def enter_query(username, password):
    try:
        my_db = mariadb.connect(user=username, password=password, database='ipmcs')
        cursor = my_db.cursor()
        sql = input("Enter MySQL command you want to make: ")
        cursor.execute(sql)
        result = cursor.fetchall()
        for row in result:
            print(row)
        my_db.commit()
        sqldump()
    except mariadb.Error as error:
        my_db.rollback()  # rollback if any exception occured
        print("Wrong query {}".format(error))
    pass


main()





-- MySQL dump 10.17  Distrib 10.3.14-MariaDB, for Win64 (AMD64)
--
-- Host: localhost    Database: iPMCS
-- ------------------------------------------------------
-- Server version	10.3.14-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `hmmwv`
--

DROP TABLE IF EXISTS `hmmwv`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hmmwv` (
  `item_no` int(11) NOT NULL AUTO_INCREMENT,
  `interval_` text NOT NULL,
  `location_` text NOT NULL,
  `procedure_` text NOT NULL,
  `not_FMC` text NOT NULL,
  PRIMARY KEY (`item_no`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hmmwv`
--

LOCK TABLES `hmmwv` WRITE;
/*!40000 ALTER TABLE `hmmwv` DISABLE KEYS */;
INSERT INTO `hmmwv` VALUES (1,'Before','Left Front, Side Exterior','DRIVER CAUTION If leaks are detected in the area of the transfer case oil cooler, do not attempt to tighten retaining nuts;  nternal damage to the transfer case oil cooler may result. Notify unit maintenance. Note: If leakage is detected, further investigation is needed to determine the location and cause of the leak. a. Visually check underneath vehicle for any evidence of fluid leakage. b. Visusally check front for obvious damage that would impair operation','a. Any brake fluid leak; class III leak of oil, fuel or coolant. b. Any damage that will prevent operation'),(2,'Before','Left Side Tires','DRIVER WARNING Operating a vehicle with a tire in an underinflated condition or with a questionable defect may lead to premature tire failure and may cause equipment damage and injury or death to personnel. Visually check tires for presence and under inflation. ','Tire missing, deflated, or unserviceable.'),(3,'Before','Rear Exterior','DRIVER NOTE  If leakage is detected, further investigation is needed to determine the location and cause of the leak. a. Visually check underneath vehicle for evidence of fluid leakage. b. Visually check rear of vehicle for obvious damage that would impair operation. c. Inspect bumper supports for cracks before towing trailer.','a. Any brake fluid leak; class III leak of oil, fuel, or coolant. b. Any damage that would prevent operation. c. Any damage that would prevent operation.'),(4,'Before','Exhaust Louvers','(M996 and M996A1 only) Louvers Check air exhaust louvers to ensure they are clear and free of debris that would restrict air flow. Clean any dirt or debris from louvers.',''),(5,'Before','Right Front Side Exterior','DRIVER NOTE If leakage is detected, further investigation is needed to determine the location and cause of the leak. a. Visually check underneath vehicle for evidence of fluid leakage b. Visually check front and right side of vehicle for obvious damage that would inpair operation.','a. Any brake fluid leak; class III leak of oil, fuel, or coolant. b. Any damage right side of vehicle for obvious damage that would operation.');
/*!40000 ALTER TABLE `hmmwv` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `user_id` varchar(255) NOT NULL,
  `u_password` varchar(255) NOT NULL,
  `is_Admin` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES ('123','123',1),('213432424','2342343',0),('a','b',1),('ALIBABA','ALLL',0),('ariel','ariel',1),('fer','fer2',1),('frek','frek',1),('heeeeel','lee',1),('hello','hello',1),('help','help',1),('ksjdfh','22',1),('ramses','machado',1),('rert','rert',1),('sseee2','1',1),('test','test',1),('testinfewerwr','fdsfsdf',1),('testing','xxxxxxxxxxx',0),('TESTINGARIEL','TEST',1),('xarielx','1',1),('xarielx506','root',1),('xde','xde',1),('xxxxxx','xxx',0),('XXXXXXXXXXXXXXXXXXXAAAAAAAAAAAAAAAAAAAA','XXXXXXXX',1);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vehicle`
--

DROP TABLE IF EXISTS `vehicle`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vehicle` (
  `vehicle_type` varchar(255) NOT NULL,
  `vehicle_id` varchar(255) NOT NULL,
  `FMC_Status` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`vehicle_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vehicle`
--

LOCK TABLES `vehicle` WRITE;
/*!40000 ALTER TABLE `vehicle` DISABLE KEYS */;
INSERT INTO `vehicle` VALUES ('hmmwv','33',1);
/*!40000 ALTER TABLE `vehicle` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-06-26  2:02:07
